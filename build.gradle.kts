/*
 * SPDX-FileCopyrightText: © 2020 Sebastian Davids <sdavids@gmx.de>
 * SPDX-License-Identifier: Apache-2.0
 */

// https://docs.gradle.org/current/userguide/kotlin_dsl.html

import com.github.spotbugs.snom.Confidence
import com.github.spotbugs.snom.Effort
import net.ltgt.gradle.errorprone.errorprone
import net.ltgt.gradle.nullaway.nullaway
import java.util.Locale

plugins {
  id("de.sdavids.commons.build-parameters")
  id("de.sdavids.commons.build-metadata")
  id("de.sdavids.commons.testing-conventions")
  `java-library`
  `maven-publish`
  signing
  checkstyle
  pmd
  jacoco
  alias(libs.plugins.nemerosaVersioning)
  alias(libs.plugins.spotless)
  alias(libs.plugins.spotlessChangelog)
  alias(libs.plugins.errorprone)
  alias(libs.plugins.nullaway)
  alias(libs.plugins.forbiddenapis)
  alias(libs.plugins.spotbugs)
}

configurations {
  compileOnlyApi {
    resolutionStrategy.failOnVersionConflict()
  }
  implementation {
    resolutionStrategy.failOnVersionConflict()
  }
  api {
    resolutionStrategy.failOnVersionConflict()
  }
}

dependencies {
  errorprone(libs.errorprone.core)
  errorprone(libs.nullaway)
  checkstyle(libs.checkstyle)
  spotbugsPlugins(libs.findsecbugs)
  spotbugsPlugins(libs.sbContrib)

  api(libs.apiguardianApi)
  api(libs.jspecify)

  testRuntimeOnly(libs.log4j.core)
  testRuntimeOnly(libs.log4j.jul)
}

spotlessChangelog {
  changelogFile("CHANGELOG.adoc")
  ifFoundBumpBreaking("*BREAKING*")
  ifFoundBumpAdded("=== Added")
  setAppendDashSnapshotUnless_dashPrelease(true)
}

group = "de.sdavids.commons.test"
version = spotlessChangelog.versionNext
description = "Utility methods and classes for writing tests."

versioning {
  releases = setOf("main")
  dirtyFailOnReleases = buildParameters.release
}

java {
  toolchain {
    languageVersion = JavaLanguageVersion.of(17)
  }

  consistentResolution {
    useCompileClasspathVersions()
  }

  withJavadocJar()
  withSourcesJar()
}

tasks.withType<JavaCompile>().configureEach {
  options.apply {
    release = 8
    isFork = true
    encoding = "UTF-8"
    debugOptions.debugLevel = "source,lines,vars"
    isDeprecation = true
    compilerArgs.addAll(
      arrayOf(
        "-parameters",
        "-Xlint:all",
      ),
    )
    errorprone.apply {
      allDisabledChecksAsWarnings = true
      // https://errorprone.info/bugpatterns
      disable(
        "AndroidJdkLibsChecker",
        "ImmutableMemberCollection",
        "Java7ApiChecker",
        "Var",
        "Varifier",
      )
      nullaway.apply {
        annotatedPackages.add("de.sdavids.commons.test")
        if (name.lowercase(Locale.ROOT).contains("test")) {
          disable()
        } else {
          error()
        }
      }
    }
  }
}

tasks.withType<AbstractArchiveTask>().configureEach {
  isPreserveFileTimestamps = false
  isReproducibleFileOrder = true
  filePermissions {
    unix("rw-r--r--")
  }
  dirPermissions {
    unix("rwxr-xr-x")
  }
  from(projectDir) {
    include(
      "COPYRIGHT",
      "NOTICE",
      "LICENSE",
    )
    into("META-INF")
  }
}

val buildDate: String by rootProject.extra
val buildTime: String by rootProject.extra

@Suppress("ktlint")
tasks.withType<Jar>().configureEach {
  manifest {
    attributes(
      "Build-Revision" to versioning.info.commit,
      "Build-Date" to buildDate,
      "Build-Time" to buildTime,
      "Built-By" to "Sebastian Davids",
      "Created-By" to "${System.getProperty("java.version")} (${System.getProperty("java.vendor")} ${System.getProperty("java.vm.version")})",
      "Specification-Title" to "sdavids-commons-test",
      "Specification-Version" to version,
      "Specification-Vendor" to "Sebastian Davids",
      "Implementation-Title" to "sdavids-commons-test",
      "Implementation-Version" to "$version-${versioning.info.build}",
      "Implementation-Vendor" to "Sebastian Davids",
      "Automatic-Module-Name" to "de.sdavids.commons.test",
    )
  }
}

spotless {
  java {
    removeUnusedImports()
    googleJavaFormat(libs.versions.googleJavaFormat.get())
    formatAnnotations()
    licenseHeaderFile("gradle/config/spotless/apache-public-license-2.0-java.txt")
  }
  kotlinGradle {
    target(
      "build.gradle.kts",
      "settings.gradle.kts",
    )
    ktlint(libs.versions.ktlint.get())
  }
  format("sh") {
    target("**/*.sh")
    trimTrailingWhitespace()
    endWithNewline()
  }
  format("adoc") {
    target("**/*.adoc")
    trimTrailingWhitespace()
    endWithNewline()
  }
  format("md") {
    target("**/*.md")
    endWithNewline()
  }
  format("properties") {
    target("gradle.properties", "src/**/*.properties")
    trimTrailingWhitespace()
    endWithNewline()
  }
  format("xml") {
    target("gradle/**/*.xml", "src/**/*.xml")
    leadingTabsToSpaces(2)
    trimTrailingWhitespace()
    endWithNewline()
  }
  format("yml") {
    target("**/*.yml")
    leadingTabsToSpaces(2)
    trimTrailingWhitespace()
    endWithNewline()
  }
  format("misc") {
    target(
      ".editorconfig",
      ".gitattributes",
      ".gitignore",
      ".mailmap",
      ".sdkmanrc",
      "AUTHORS",
      "CONTRIBUTORS",
      "COPYRIGHT",
      "LICENSE",
      "NOTICE",
      "OSSMETADATA",
      "TODO",
    )
    trimTrailingWhitespace()
    endWithNewline()
  }
}

@Suppress("ktlint")
tasks.javadoc {
  options.apply {
    locale = "en_US"
    encoding = "UTF-8"
    memberLevel = JavadocMemberLevel.PROTECTED
    header = "sdavids-commons-test $version"
    (this as StandardJavadocDocletOptions).apply {
      docTitle = "sdavids-commons-test API"
      links = listOf(
        "https://docs.oracle.com/en/java/javase/17/docs/api/",
        "https://apiguardian-team.github.io/apiguardian/docs/${libs.versions.apiguardian.get()}/api/",
      )
      noTimestamp(true)
      addBooleanOption("Xdoclint:all", true)
    }
  }
}

forbiddenApis {
  bundledSignatures =
    setOf(
      "jdk-unsafe-1.8",
      "jdk-deprecated-1.8",
      "jdk-internal-1.8",
      "jdk-non-portable",
      "jdk-system-out",
      "jdk-reflection",
    )
}

tasks.withType<Checkstyle>().configureEach {
  config = resources.text.fromFile("gradle/config/checkstyle/checkstyle.xml")
  maxWarnings = 0
  reports {
    xml.required = false
  }
}

pmd {
  toolVersion = libs.versions.pmd.get()
  incrementalAnalysis = true
  ruleSetConfig = resources.text.fromFile("gradle/config/pmd/pmd.xml")
  threads = Runtime.getRuntime().availableProcessors()
}

spotbugs {
  toolVersion = libs.versions.spotbugs.get()
  effort = Effort.MAX
  reportLevel = Confidence.LOW
  excludeFilter = file("gradle/config/spotbugs/spotbugs-exclude-filter.xml")
}

tasks.withType<com.github.spotbugs.snom.SpotBugsTask>().configureEach {
  reports.maybeCreate("xml").required = false
  reports.maybeCreate("html").required = true
}

jacoco {
  toolVersion = libs.versions.jacoco.get()
}

tasks.jacocoTestReport {
  dependsOn(tasks.test)
}

tasks.check {
  dependsOn(tasks.jacocoTestReport)
}

@Suppress("ktlint")
publishing {
  publications {
    create<MavenPublication>("mavenJava") {
      from(components["java"])
      withoutBuildIdentifier()
      pom {
        name = "sdavids-commons-test"
        description = "Utility methods and classes for writing tests."
        url = "https://gitlab.com/sebastiandavids/sdavids-commons-test"
        inceptionYear = "2017"
        licenses {
          license {
            name = "Apache License, Version 2.0"
            url = "https://www.apache.org/licenses/LICENSE-2.0.txt"
            distribution = "repo"
          }
        }
        developers {
          developer {
            id = "sdavids"
            name = "Sebastian Davids"
            email = "sdavids@gmx.de"
            timezone = "Europe/Berlin"
          }
        }
        scm {
          url = "https://gitlab.com/sebastiandavids/sdavids-commons-test"
          connection = "scm:git:https://gitlab.com/sebastiandavids/sdavids-commons-test.git"
          developerConnection = "scm:git:https://gitlab.com/sebastiandavids/sdavids-commons-test.git"
        }
        issueManagement {
          system = "GitLab"
          url = "https://gitlab.com/sebastiandavids/sdavids-commons-test/issues"
        }
      }
    }
  }
}

val isSnapshot = version.toString().contains("SNAPSHOT")

signing {
  useGpgCmd()
  sign(publishing.publications)
  isRequired = !isSnapshot
}

tasks.withType<Sign>().configureEach {
  onlyIf {
    !isSnapshot
  }
}

defaultTasks("spotlessApply", "build")
