/*
 * SPDX-FileCopyrightText: © 2024 Sebastian Davids <sdavids@gmx.de>
 * SPDX-License-Identifier: Apache-2.0
 */

// https://docs.gradle.org/current/userguide/kotlin_dsl.html

plugins {
  alias(libs.plugins.buildParameters)
}

group = "de.sdavids.commons"

// https://gradlex.org/build-parameters/#usage
buildParameters {
  pluginId("de.sdavids.commons.build-parameters")
  bool("ci") {
    description = "Whether or not this build is running in a CI environment"
    defaultValue = false
    fromEnvironment()
  }
  bool("release") {
    description = "Whether or not this build is a release build"
    defaultValue = false
  }
}
