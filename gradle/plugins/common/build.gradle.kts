/*
 * SPDX-FileCopyrightText: © 2024 Sebastian Davids <sdavids@gmx.de>
 * SPDX-License-Identifier: Apache-2.0
 */

plugins {
  `kotlin-dsl`
}

repositories {
  gradlePluginPortal()
}

dependencies {
  implementation(projects.buildParameters)
  implementation(kotlin("gradle-plugin"))
  implementation(libs.gradle.develocity)
  implementation(libs.gradle.foojayResolver)
}

group = "de.sdavids.commons"
