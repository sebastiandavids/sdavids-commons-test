/*
 * SPDX-FileCopyrightText: © 2024 Sebastian Davids <sdavids@gmx.de>
 * SPDX-License-Identifier: Apache-2.0
 */

import org.gradle.api.Project
import org.gradle.api.artifacts.VersionCatalog
import org.gradle.api.artifacts.VersionCatalogsExtension
import org.gradle.kotlin.dsl.the

fun Project.bundleFromLibs(name: String) =
  libsVersionCatalog.findBundle(name).get()

fun Project.dependencyFromLibs(name: String) =
  libsVersionCatalog.findLibrary(name).get()

fun Project.requiredVersionFromLibs(name: String) =
  libsVersionCatalog.findVersion(name).get().requiredVersion

private val Project.libsVersionCatalog: VersionCatalog
  get() = the<VersionCatalogsExtension>().named("libs")
