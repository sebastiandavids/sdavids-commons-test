/*
 * SPDX-FileCopyrightText: © 2024 Sebastian Davids <sdavids@gmx.de>
 * SPDX-License-Identifier: Apache-2.0
 */

import java.time.Instant
import java.time.OffsetDateTime
import java.time.ZoneOffset.UTC
import java.time.format.DateTimeFormatter.ISO_LOCAL_DATE
import java.time.format.DateTimeFormatter.ISO_OFFSET_TIME
import java.time.temporal.ChronoUnit.SECONDS

// https://reproducible-builds.org/docs/source-date-epoch/
val buildTimeAndDate: OffsetDateTime by extra {
  OffsetDateTime.ofInstant(
    (System.getenv("SOURCE_DATE_EPOCH") ?: "").toLongOrNull()?.let {
      Instant.ofEpochSecond(it)
    } ?: Instant.now().truncatedTo(SECONDS),
    UTC,
  )
}
val buildDate: String by extra { ISO_LOCAL_DATE.format(buildTimeAndDate) }
val buildTime: String by extra { ISO_OFFSET_TIME.format(buildTimeAndDate) }
