/*
 * SPDX-FileCopyrightText: © 2024 Sebastian Davids <sdavids@gmx.de>
 * SPDX-License-Identifier: Apache-2.0
 */

import org.gradle.api.plugins.jvm.JvmTestSuite

plugins {
  `java-library`
  id("de.sdavids.commons.build-parameters")
}

testing {
  suites {
    val test by getting(JvmTestSuite::class) {
      useJUnitJupiter(requiredVersionFromLibs("junit"))
      dependencies {
        implementation(dependencyFromLibs("assertj-core"))
      }
      targets {
        all {
          testTask.configure {
            // see comment in gradle.properties
            systemProperty("user.timezone", System.getProperty("user.timezone"))
            systemProperty("user.language", System.getProperty("user.language"))
            systemProperty("user.country", System.getProperty("user.country"))
            systemProperty("file.encoding", System.getProperty("file.encoding"))
            systemProperty("java.awt.headless", "true")
            // https://logging.apache.org/log4j/2.x/log4j-jul.html
            systemProperty("java.util.logging.manager", "org.apache.logging.log4j.jul.LogManager")
            // https://logging.apache.org/log4j/2.x/log4j-jul.html#log4j2.julLoggerAdapter
            systemProperty("log4j2.julLoggerAdapter", "org.apache.logging.log4j.jul.CoreLoggerAdapter")

            failFast = !buildParameters.ci

            develocity.testRetry {
              maxRetries = if (buildParameters.ci) 3 else 0
              failOnPassedAfterRetry = true
            }

            reports.junitXml.required = false

            testLogging {
              exceptionFormat = org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
            }
          }
        }
      }
    }
  }
}
