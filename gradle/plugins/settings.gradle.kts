/*
 * SPDX-FileCopyrightText: © 2024 Sebastian Davids <sdavids@gmx.de>
 * SPDX-License-Identifier: Apache-2.0
 */

// https://docs.gradle.org/current/dsl/org.gradle.api.initialization.Settings.html

val expectedJavaVersion = JavaVersion.VERSION_17
val actualJavaVersion = JavaVersion.current()
require(actualJavaVersion.isCompatibleWith(expectedJavaVersion)) {
  "The build requires Java ${expectedJavaVersion.majorVersion} or higher. Currently executing with Java ${actualJavaVersion.majorVersion}."
}

dependencyResolutionManagement {
  versionCatalogs {
    create("libs") {
      from(files("../libs.versions.toml"))
    }
  }
}

rootProject.name = "plugins"

include("build-parameters")
include("common")

enableFeaturePreview("STABLE_CONFIGURATION_CACHE")
enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")
