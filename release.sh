#!/usr/bin/env sh

# SPDX-FileCopyrightText: © 2017 Sebastian Davids <sdavids@gmx.de>
# SPDX-License-Identifier: Apache-2.0

# https://docs.gradle.org/current/userguide/signing_plugin.html#example_configure_the_gnupgsignatory
#
# Modify ~/.gradle/gradle.properties:
#
# signing.gnupg.keyName=...
#

set -eu

# https://reproducible-builds.org/docs/source-date-epoch/#git
SOURCE_DATE_EPOCH="${SOURCE_DATE_EPOCH:-$(git log --max-count=1 --pretty=format:%ct)}"
export SOURCE_DATE_EPOCH

./gradlew \
  --no-configuration-cache \
  --no-build-cache \
  --rerun-tasks \
  -Prelease=true \
  clean \
  build \
  changelogBump \
  publishToMavenLocal

version="v$(./gradlew --quiet changelogPrint | tail --lines 1 | sed 's/^[a-z-]* \([0-9.]*\) .*$/\1/')"

git commit --signoff --gpg-sign --all --message "Publish ${version}"
git tag --sign --annotate "${version}" --message "${version}"
git push origin
git push origin "${version}"
