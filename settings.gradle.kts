/*
 * SPDX-FileCopyrightText: © 2020 Sebastian Davids <sdavids@gmx.de>
 * SPDX-License-Identifier: Apache-2.0
 */

// https://docs.gradle.org/current/dsl/org.gradle.api.initialization.Settings.html

import buildparameters.BuildParametersExtension

val expectedJavaVersion = JavaVersion.VERSION_17
val actualJavaVersion = JavaVersion.current()
require(actualJavaVersion.isCompatibleWith(expectedJavaVersion)) {
  "The build requires Java ${expectedJavaVersion.majorVersion} or higher. Currently executing with Java ${actualJavaVersion.majorVersion}."
}

pluginManagement {
  includeBuild("gradle/plugins")
  repositories {
    gradlePluginPortal()
  }
}

plugins {
  id("de.sdavids.commons.build-parameters")
  id("de.sdavids.commons.settings-conventions")
}

dependencyResolutionManagement {
  repositories {
    mavenCentral()
  }
  repositoriesMode = RepositoriesMode.FAIL_ON_PROJECT_REPOS
}

rootProject.name = "sdavids-commons-test"

val buildParameters = the<BuildParametersExtension>()

develocity {
  buildScan {
    termsOfUseUrl = "https://gradle.com/help/legal-terms-of-use"
    termsOfUseAgree = "yes"
    obfuscation {
      username { null }
      hostname { null }
      ipAddresses { emptyList() }
    }

    if (buildParameters.ci) {
      uploadInBackground = false
      publishing.onlyIf { true }
    } else {
      publishing.onlyIf { false }
    }
  }
}

enableFeaturePreview("STABLE_CONFIGURATION_CACHE")
enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")
