/*
 * SPDX-FileCopyrightText: © 2017 Sebastian Davids <sdavids@gmx.de>
 * SPDX-License-Identifier: Apache-2.0
 */
package de.sdavids.commons.test;

import static java.lang.String.format;
import static java.lang.reflect.Modifier.isAbstract;
import static java.lang.reflect.Modifier.isPublic;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Arrays.stream;
import static java.util.Collections.unmodifiableSet;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toSet;
import static org.apiguardian.api.API.Status.STABLE;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.lang.invoke.MethodHandles;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.Callable;
import org.apiguardian.api.API;
import org.jspecify.annotations.Nullable;

/**
 * Enables one to register mock services.
 *
 * <h2>Notes</h2>
 *
 * <p>Tasks executed via {@link java.util.concurrent.Executor}, {@link
 * java.util.concurrent.ExecutorService}, or {@link java.util.concurrent.CompletionService} may not
 * see the registered mock services.
 *
 * <p>Tasks executed with either the {@link java.util.concurrent.Executors#defaultThreadFactory()}
 * or the {@link java.util.concurrent.ForkJoinPool#commonPool()} will not see the registered mock
 * services.
 *
 * <h2 id="usage-set-services">Usage JVM-global</h2>
 *
 * <pre><code>
 * &#64;AfterEach
 * void tearDown() {
 *   // clears all mock services
 *   MockServices.setServices();
 * }
 *
 * &#64;Test
 * void setServices() {
 *   MockServices.setServices(MyServiceMock.class);
 *
 *   Iterator&lt;MyService&gt; providers = ServiceLoader.load(MyService.class).iterator();
 *
 *   // providers.next() is MyServiceMock
 * }
 * </code></pre>
 *
 * <h2 id="usage-current-thread">Usage Current Thread</h2>
 *
 * <pre><code>
 * &#64;Test
 * void withServicesForRunnableInCurrentThread() {
 *   MockServices.withServicesForRunnableInCurrentThread(() -&gt; {
 *     Iterator&lt;MyService&gt; providers = ServiceLoader.load(MyService.class).iterator();
 *
 *     // providers.next() is MyServiceMock
 *   }, MyServiceMock.class);
 * }
 *
 * &#64;Test
 * void withServicesForCallableInCurrentThread() {
 *   MockServices.withServicesForCallableInCurrentThread(() -&gt; {
 *     Iterator&lt;MyService&gt; providers = ServiceLoader.load(MyService.class).iterator();
 *
 *     // providers.next() is MyServiceMock
 *
 *     return null;
 *   }, MyServiceMock.class);
 * }
 * </code></pre>
 *
 * @see java.util.ServiceLoader
 * @since 1.0
 */
@SuppressWarnings("PMD.DoNotUseThreads")
@API(status = STABLE, since = "1.0")
public final class MockServices {

  private MockServices() {}

  /**
   * Sets (or resets) the mock services.
   *
   * <p>Clears any previous mock service registrations.
   *
   * <p>Service implementations registered via {@code META-INF/services/} are available after the
   * ones registered by this method.
   *
   * <p>Each mock service class must be public and have a public no-arg constructor.
   *
   * @param services the mock services; not null
   * @since 1.0
   */
  @SuppressWarnings({
    "PMD.AvoidInstantiatingObjectsInLoops",
    "PMD.AvoidThreadGroup",
    "PMD.CognitiveComplexity",
    "checkstyle:MethodLength",
    "ObjectAllocationInLoop",
  })
  public static void setServices(Class<?>... services) {
    requireNonNull(services, "services");

    @SuppressWarnings("ClassLoaderInstantiation")
    ClassLoader loader = new ServiceClassLoader(services);

    ThreadGroup threadGroup = Thread.currentThread().getThreadGroup();
    while (threadGroup.getParent() != null) {
      threadGroup = threadGroup.getParent();
    }

    while (true) {
      int count = threadGroup.activeCount() + 1;
      Thread[] threads = new Thread[count];
      int threadCount = threadGroup.enumerate(threads, true);
      if (threadCount < count) {
        for (int i = 0; i < threadCount; i++) {
          String threadName = threads[i].getClass().getName();
          if (threadName.endsWith("InnocuousThread")
              || threadName.endsWith("InnocuousForkJoinWorkerThread")) {
            continue;
          }

          try {
            threads[i].setContextClassLoader(loader);
          } catch (SecurityException ignored) {
            // ignored
          }
        }
        break;
      }
    }
  }

  /**
   * Sets (or resets) the mock services for the current thread.
   *
   * <p>Clears any previous mock service registrations of the current thread.
   *
   * <p>Service implementations registered via {@code META-INF/services/} are available after the
   * ones registered by this method.
   *
   * <p>Each mock service class must be public and have a public no-arg constructor.
   *
   * @param services the mock services; not null
   * @since 3.0
   */
  @API(status = STABLE, since = "3.0")
  public static void setServicesForCurrentThread(Class<?>... services) {
    internalSetServicesForThread(Thread.currentThread(), services);
  }

  /**
   * Sets (or resets) the mock services for the given thread.
   *
   * <p>Clears any previous mock service registrations of that thread.
   *
   * <p>Service implementations registered via {@code META-INF/services/} are available after the
   * ones registered by this method.
   *
   * <p>Each mock service class must be public and have a public no-arg constructor.
   *
   * @param thread the thread; not null
   * @param services the mock services; not null
   * @since 3.0
   */
  @API(status = STABLE, since = "3.0")
  public static void setServicesForThread(Thread thread, Class<?>... services) {
    internalSetServicesForThread(requireNonNull(thread, "thread"), services);
  }

  /**
   * Sets the mock services for the current thread and executes the given {@link Runnable} task.
   *
   * <p>Clears any previous mock service registrations before executing the given task; resets the
   * previous service registrations afterward.
   *
   * <p>Service implementations registered via {@code META-INF/services/} are available after the
   * ones registered by this method.
   *
   * <p>Each mock service class must be public and have a public no-arg constructor.
   *
   * <p><em>Note:</em> Threads started within the task will also have the mock services registered.
   *
   * <p>Example:
   *
   * <pre>{@code
   * MockServices.withServicesForRunnableInCurrentThread(() -> {
   *   Iterator<MyService> providers  = ServiceLoader.load(MyService.class).iterator();
   *
   *   // providers.next() is MyServiceMock
   *
   *   Thread t = new Thread(() -> {
   *     Iterator<MyService> providers = ServiceLoader.load(MyService.class).iterator();
   *
   *     // providers.next() is MyServiceMock
   *   }).start();
   *   t.join();
   *
   *   return null;
   * },
   * MyServiceMock.class);
   * }</pre>
   *
   * @param runnable the runnable task; not null
   * @param services the mock services; not null
   * @since 2.0
   */
  @API(status = STABLE, since = "2.0")
  public static void withServicesForRunnableInCurrentThread(
      Runnable runnable, Class<?>... services) {

    withServicesForRunnableInThread(Thread.currentThread(), runnable, services);
  }

  /**
   * Sets the mock services for the current thread and executes the given {@link Callable} task.
   *
   * <p>Clears any previous mock service registrations before executing the given task; resets the
   * previous service registrations afterward.
   *
   * <p>Service implementations registered via {@code META-INF/services/} are available after the
   * ones registered by this method.
   *
   * <p>Each mock service class must be public and have a public no-arg constructor.
   *
   * <p><em>Note:</em> Threads started within the task will also have the mock services registered.
   *
   * <p>Example:
   *
   * <pre>{@code
   * MockServices.withServicesForCallableInCurrentThread(() -> {
   *   Iterator<MyService> providers  = ServiceLoader.load(MyService.class).iterator();
   *
   *   // providers.next() is MyServiceMock
   *
   *   Thread t = new Thread(() -> {
   *     Iterator<MyService> providers = ServiceLoader.load(MyService.class).iterator();
   *
   *     // providers.next() is MyServiceMock
   *   }).start();
   *   t.join();
   *
   *   return null;
   * },
   * MyServiceMock.class);
   * }</pre>
   *
   * @param callable the callable task; not null
   * @param services the mock services; not null
   * @throws Exception if the given callable throws an exception
   * @since 2.0
   */
  @SuppressWarnings({
    "ProhibitedExceptionDeclared",
    "PMD.SignatureDeclareThrowsException",
  })
  @API(status = STABLE, since = "2.0")
  public static void withServicesForCallableInCurrentThread(
      Callable<?> callable, Class<?>... services) throws Exception {

    withServicesForCallableInThread(Thread.currentThread(), callable, services);
  }

  private static void withServicesForRunnableInThread(
      Thread thread, Runnable runnable, Class<?>... services) {

    requireNonNull(thread, "thread");
    requireNonNull(runnable, "runnable");
    requireNonNull(services, "services");

    ClassLoader contextClassLoader = thread.getContextClassLoader();

    boolean contextClassLoaderSet = internalSetServicesForThread(thread, services);

    try {
      runnable.run();
    } finally {
      if (contextClassLoaderSet) {
        try {
          thread.setContextClassLoader(contextClassLoader);
        } catch (SecurityException e) {
          // ignore
        }
      }
    }
  }

  @SuppressWarnings({
    "ProhibitedExceptionDeclared",
    "PMD.SignatureDeclareThrowsException",
  })
  private static void withServicesForCallableInThread(
      Thread thread, Callable<?> callable, Class<?>... services) throws Exception {

    requireNonNull(thread, "thread");
    requireNonNull(callable, "callable");
    requireNonNull(services, "services");

    ClassLoader contextClassLoader = thread.getContextClassLoader();

    boolean contextClassLoaderSet = internalSetServicesForThread(thread, services);

    try {
      callable.call();
    } finally {
      if (contextClassLoaderSet) {
        try {
          thread.setContextClassLoader(contextClassLoader);
        } catch (SecurityException e) {
          // ignore
        }
      }
    }
  }

  private static boolean internalSetServicesForThread(Thread thread, Class<?>... services) {
    String threadName = thread.getClass().getName();
    if (threadName.endsWith("InnocuousThread")
        || threadName.endsWith("InnocuousForkJoinWorkerThread")) {
      return false;
    }

    try {
      //noinspection ClassLoaderInstantiation
      thread.setContextClassLoader(new ServiceClassLoader(services));
    } catch (SecurityException e) {
      return false;
    }

    return true;
  }

  @SuppressWarnings("CustomClassloader")
  private static final class ServiceClassLoader extends ClassLoader {

    private static final String META_INF_SERVICES = "META-INF/services/";

    private final Set<Class<?>> services;

    @SuppressWarnings({
      "ReturnValueIgnored",
      "PMD.UseProperClassLoader",
    })
    ServiceClassLoader(Class<?>... services) {
      super(MethodHandles.lookup().lookupClass().getClassLoader());

      Set<Class<?>> classes = stream(services).collect(toSet());

      for (Class<?> clazz : classes) {
        try {
          int mods = clazz.getModifiers();
          if (!isPublic(mods) || isAbstract(mods)) {
            throw new IllegalArgumentException(
                format(
                    Locale.ROOT, "Class %s must be a public non-abstract class", clazz.getName()));
          }
          clazz.getConstructor();
        } catch (NoSuchMethodException e) {
          throw new IllegalArgumentException(
              format(Locale.ROOT, "Class %s has no public no-arg constructor", clazz.getName()), e);
        }
      }

      this.services = unmodifiableSet(classes);
    }

    @SuppressWarnings("JdkObsolete")
    @Override
    public @Nullable URL getResource(String name) {
      try {
        Enumeration<URL> resources = getResources(name);
        return resources.hasMoreElements() ? resources.nextElement() : null;
      } catch (IOException ignored) {
        return null;
      }
    }

    @SuppressWarnings("checkstyle:MethodLength")
    @Override
    public Enumeration<URL> getResources(String name) throws IOException {
      Enumeration<URL> resources = super.getResources(name);

      if (!name.startsWith(META_INF_SERVICES)) {
        return resources;
      }

      Class<?> serviceClass;
      try {
        serviceClass = loadClass(name.substring(META_INF_SERVICES.length()));
      } catch (ClassNotFoundException ignored) {
        return resources;
      }

      Set<String> impls =
          services.stream()
              .filter(serviceClass::isAssignableFrom)
              .map(Class::getName)
              .collect(toSet());

      if (impls.isEmpty()) {
        return resources;
      }

      try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
          PrintWriter writer = new PrintWriter(new OutputStreamWriter(outputStream, UTF_8))) {
        for (String impl : impls) {
          writer.println(impl);
          writer.println("#position=100");
        }
        writer.flush();
        return new ServiceEnumeration(resources, serviceClass, outputStream);
      }
    }
  }

  @SuppressWarnings({
    "JdkObsolete",
    "PMD.ReplaceEnumerationWithIterator",
  })
  private static final class ServiceEnumeration implements Enumeration<URL> {

    private final Enumeration<URL> resources;
    private final URL url;

    private boolean parent;

    ServiceEnumeration(
        Enumeration<URL> resources, Class<?> serviceClass, ByteArrayOutputStream baos)
        throws MalformedURLException {

      this.resources = resources;
      url =
          new URL(
              "metainfservices", null, 0, serviceClass.getName(), new ServiceStreamHandler(baos));
    }

    @Override
    public boolean hasMoreElements() {
      return !parent || resources.hasMoreElements();
    }

    @Override
    public URL nextElement() {
      if (parent) {
        return resources.nextElement();
      } else {
        parent = true;
        return url;
      }
    }
  }

  private static final class ServiceStreamHandler extends URLStreamHandler {

    private final byte[] bytes;

    ServiceStreamHandler(ByteArrayOutputStream outputStream) {
      bytes = outputStream.toByteArray();
    }

    @Override
    protected URLConnection openConnection(URL url) {
      return new ServiceStreamHandlerUrlConnection(url, bytes);
    }

    private static final class ServiceStreamHandlerUrlConnection extends URLConnection {

      private final byte[] bytes;

      ServiceStreamHandlerUrlConnection(URL url, byte[] bytes) {
        super(url);
        this.bytes = requireNonNull(bytes, "bytes");
      }

      @Override
      public void connect() {
        // ignore
      }

      @Override
      public InputStream getInputStream() {
        return new ByteArrayInputStream(bytes);
      }
    }
  }
}
