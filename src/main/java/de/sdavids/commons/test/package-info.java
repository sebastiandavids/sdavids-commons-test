/*
 * SPDX-FileCopyrightText: © 2017 Sebastian Davids <sdavids@gmx.de>
 * SPDX-License-Identifier: Apache-2.0
 */

/**
 * This package contains utility methods and classes for writing tests.
 *
 * @since 1.0
 */
@NullMarked
package de.sdavids.commons.test;

import org.jspecify.annotations.NullMarked;
