/*
 * SPDX-FileCopyrightText: © 2017 Sebastian Davids <sdavids@gmx.de>
 * SPDX-License-Identifier: Apache-2.0
 */
package de.sdavids.commons.test;

public abstract class AbstractServiceInterface1 implements ServiceInterface1 {

  @Override
  public final int value() {
    return 1;
  }
}
