/*
 * SPDX-FileCopyrightText: © 2017 Sebastian Davids <sdavids@gmx.de>
 * SPDX-License-Identifier: Apache-2.0
 */
package de.sdavids.commons.test;

public final class NonPublicNoArgCtorServiceInterface1 implements ServiceInterface1 {

  NonPublicNoArgCtorServiceInterface1() {
    // do nothing
  }

  @Override
  public int value() {
    return 1;
  }
}
