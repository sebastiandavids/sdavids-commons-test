/*
 * SPDX-FileCopyrightText: © 2018 Sebastian Davids <sdavids@gmx.de>
 * SPDX-License-Identifier: Apache-2.0
 */
package de.sdavids.commons.test;

public final class StubServiceInterface1Negative implements ServiceInterface1 {

  @Override
  public int value() {
    return -1;
  }
}
