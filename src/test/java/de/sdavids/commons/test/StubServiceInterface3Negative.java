/*
 * SPDX-FileCopyrightText: © 2018 Sebastian Davids <sdavids@gmx.de>
 * SPDX-License-Identifier: Apache-2.0
 */
package de.sdavids.commons.test;

public final class StubServiceInterface3Negative implements ServiceInterface3 {

  @Override
  public int value() {
    return -3;
  }
}
