/*
 * SPDX-FileCopyrightText: © 2018 Sebastian Davids <sdavids@gmx.de>
 * SPDX-License-Identifier: Apache-2.0
 */
package de.sdavids.commons.test;

import static java.lang.Thread.currentThread;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ExtensionContext.Namespace;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;

@SuppressWarnings("PMD.DoNotUseThreads")
public final class WithThreadExtension
    implements AfterEachCallback, BeforeEachCallback, ParameterResolver {

  private static final Namespace WITH_THREAD = Namespace.create(WithThreadExtension.class);
  private static final String THREAD = "thread";

  @Override
  public void beforeEach(ExtensionContext context) {
    Thread thread =
        new Thread(
            () -> {
              while (!currentThread().isInterrupted()) {
                try {
                  MILLISECONDS.sleep(10L);
                } catch (InterruptedException e) {
                  // allow thread to exit
                }
              }
            },
            context.getDisplayName());
    thread.start();

    context.getStore(WITH_THREAD).put(THREAD, thread);
  }

  @Override
  public void afterEach(ExtensionContext context) {
    Thread thread = context.getStore(WITH_THREAD).remove(THREAD, Thread.class);

    if (thread == null) {
      return;
    }

    MockServices.setServicesForThread(thread);

    thread.interrupt();
  }

  @Override
  public boolean supportsParameter(
      ParameterContext parameterContext, ExtensionContext extensionContext) {

    return parameterContext.isAnnotated(WithThread.class);
  }

  @Override
  public Object resolveParameter(
      ParameterContext parameterContext, ExtensionContext extensionContext) {

    if (Thread.class.equals(parameterContext.getParameter().getType())) {
      return extensionContext.getStore(WITH_THREAD).get(THREAD, Thread.class);
    }

    throw new ParameterResolutionException(
        "Not a Thread: " + parameterContext.getParameter().getType());
  }
}
